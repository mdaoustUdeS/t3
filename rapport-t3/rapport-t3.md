---
fontsize: 12pt
lang: fr
lieu: UNIVERSITÉ DE SHERBROOKE
title: RAPPORT DE STAGE
presented-to: Sylvie Lamarche, conseillère en développement professionnel
author: |
        | Matthieu Daoust, stagiaire
        | Optel Vision inc.
        | DevOps
        | T3
date: 13 décembre 2021
geometry: margin=1in
header-includes:
    - \usepackage{times}
    - \usepackage{setspace}
    - \setstretch{1.5}
    - \usepackage{fancyhdr}
    - \pagestyle{fancy}
    - \renewcommand{\headrulewidth}{0pt}
    - \fancyhf{}
    - \lfoot{Matthieu Daoust}
    - \cfoot{\thepage}
    - \rfoot{Matthieu.Daoust@USherbrooke.ca}
---

# Partie A: Stage en développement DevOps au sein d'une équipe multidisciplinaire

## Mise en contexte

J'ai effectué mon stage dans un contexte agile au sein d'une équipe multidisciplinaire chez OPTEL dont les bureaux principaux sont situés dans la ville de Québec, au Canada. En tant que leader responsable sur le plan social et environnemental, OPTEL développe des technologies de traçabilités transformatrices pour construire un monde meilleur. Pour réussir à survivre et à continuer d'avoir un impact sur la planète, ils doivent s'assurer d'être rentable en générant plus de profits et en diminuant les coûts. L'équipe dans laquelle j'ai eu la chance de travailler pendant quatre mois participe à cet effort de diminution de coûts en migrant plusieurs ressources physiques vers des ressources virtuelles dans un écosystème infonuagique.

## Résumé du mandat

Lors de mon stage, plusieurs tâches m'ont été confiées dans le but d'aider l'équipe à migrer leurs différents répertoires de codes vers la version de GitLab offerte en tant que service (SaaS). Parmi celles-ci, j'ai facilité la migration vers GitLab des fichiers sauvegardés avec Git LFS sur un serveur interne et j'ai adapté des pipelines de Jenkins vers GitLab CI. On m'a aussi confié le mandat de m'assurer que les équipes puissent bénéficier dans GitLab des mêmes fonctionnalités qu'ils ont développées pour faciliter la gestion des revues de code (_pull request_).

## Conclusion sur mon expérience de stage

Suite à mon expérience dans une entreprise qui met en pratique ses valeurs environnementales, sociales et de saine gouvernance, je suis heureux d'avoir vécu une expérience positive. Cela a confirmé mon intérêt de travailler pour une entreprise qui a un impact réel dans la société. Cette expérience a aussi confirmé mon intérêt à aider les équipes de développement logiciel.

# Partie B

## L'environnement

> _Décrivez votre milieu de travail, indiquez vos zones de confort et d’inconfort quant à ces conditions et expliquez pourquoi._

J'ai eu la chance de travailler avec des employés qui ont l'habitude de travailler dans une équipe distribuée et j'ai adoré mon expérience. J'ai majoritairement travaillé seul, mais j'ai aussi pu partager mon expérience dans des séances de programmation en groupe (_mob programming_).

En utilisant l’expérience que j'ai acquise lors de mes stages précédents, j'ai organisé mon environnement de travail afin de réduire les inconforts et d'éviter des blessures reliées au travail de bureau. Lors des formations initiales de ce stage, j'ai aussi pu profiter de conseils ergonomiques supplémentaires pour améliorer mon environnement.

D'ailleurs, les formations initiales chez OPTEL couvrent plusieurs sujets allant des valeurs de l'entreprise aux pratiques courante, en passant par l'explication des différents produits. Ces formations m'ont permis de me familiariser avec les valeurs et les attentes de l'entreprise.

Les valeurs de l'entreprise sont d'ailleurs partagées chaque semaine par des vidéos hebdomadaires du président de l'entreprise. J'ai bien apprécié l'alignement de chaque action d'OPTEL avec ses valeurs.

## Les connaissances

> _Que savez-vous maintenant, qui vous aidera pour vos prochains stages ou votre emploi? Pourquoi?_

Durant la migration de plusieurs répertoires de codes vers GitLab, j'ai acquis plusieurs nouvelles connaissances qui pourront m'aider lors de mes prochains stages.

D'abord, j'ai pu me familiariser avec le fonctionnement interne de Git LFS. Lorsque j'ai rencontré des difficultés, je suis allé chercher de l'information à partir de plusieurs ressources, dont les notes de collègues, la documentation officielle ainsi que dans le code source lorsque certains comportements étranges n'étaient pas documentés.

Ensuite, j'ai pu adapter des pipelines de Jenkins vers GitLab CI en utilisant des fonctionnalités récentes de GitLab.

Enfin, j'ai pu approfondir mes connaissances des fonctionnalités GitLab en m'assurant que les développeurs puissent bénéficier d'un comportement similaire à ce qu'ils sont habitués pour la gestion des revues de code (_pull request_).

## Le professionnalisme

> _Avez-vous fait des apprentissages particuliers en lien avec certaines règles ou normes et quel comportement avez-vous adopté?_

Dans la plupart des entreprises, les informations sensibles et stratégiques ne sont pas habituellement partagées auprès des employés. Leur partager ces détails peut augmenter la transparence mais, si les employés ne sont pas prudents, le risque que des compétiteurs puissent utiliser ces renseignements est non négligeable.

Lors de mon stage chez OPTEL, j'ai eu la chance d'observer la confiance qui règne dans l'entreprise. En effet, l'entreprise est très transparente sur plusieurs aspects qui ne sont normalement pas diffusés aux employés. Par exemple, l'entreprise diffuse chaque semaine des statistiques par rapport aux ventes, et périodiquement, ils mettent à jour un tableau de bord de plusieurs indicateurs clés de performance.

J'ai apprécié la grande confiance d'OPTEL envers ses employés afin qu'ils conservent la nature confidentielle de ces informations. Cela renforce la confiance du personnel.

## La connaissance de soi

> _Parlez-nous de vos valeurs et de leur rôle dans cette expérience._

Durant mon stage, je me suis rendu compte que je partageais des valeurs similaires à celles de mon employeur. En effet, les valeurs d'OPTEL sont la responsabilité sociale et environnementale, l'agilité, le respect d'autrui, la collaboration, la qualité et le respect des engagements. J'ai pu vivre et apprécier ces valeurs pendant mon expérience de stage.

D'abord, j'ai apprécié les nombreux comités et les nombreuses initiatives en matière de responsabilité sociale et environnementale. Cela démontre que l'employeur a coeur le développement durable et le bien-être de ses employés et mets en pratique ses valeurs environnementales, sociales et de saine gouvernance.

Ensuite, j'ai eu la chance de participer à des échanges respectueux entre mes collègues de travail. Ces échanges ont fait en sorte que le milieu de travail était sain et agréable.

Enfin, j'ai su profiter de la rétroaction de mes collègues. Cette agilité et cette collaboration m'ont permis de m'adapter tout au long de mon stage et de remettre un travail de qualité.
